export default interface product {
  id?: number;

  name: string;

  price: number;

  createAt?: Date;

  updatedAt?: Date;

  deletedAt?: Date;
}
