import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
import productService from "@/services/product";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useProductStore = defineStore("Product", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const checkDialog = ref();
  const deleteDialog = ref(false);
  const products = ref<Product[]>([]);
  const editedProduct = ref<Product>({ name: "", price: 0 });

  watch(dialog, (newdialog, oldDialog) => {
    console.log(newdialog);
    if (!newdialog) {
      editedProduct.value = { name: "", price: 0 };
    }
  });
  async function getProducts() {
    loadingStore.isloading = true;
    try {
      const res = await productService.getProduct();
      products.value = res.data;
      console.log(res);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Product ได้");
    }
    loadingStore.isloading = false;
  }
  async function saveProduct() {
    loadingStore.isloading = true;

    try {
      if (editedProduct.value.id) {
        const res = await productService.updateProduct(
          editedProduct.value.id,
          editedProduct.value
        );
      } else {
        const res = await productService.saveProduct(editedProduct.value);
      }
      dialog.value = false;
      await getProducts();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Product ได้");
    }
    loadingStore.isloading = false;
  }
  async function deleteProduct(id: number) {
    loadingStore.isloading = true;

    try {
      const res = await productService.deleteProduct(id);
      await getProducts();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล Product ได้");
    }
    loadingStore.isloading = false;
  }

  function editProduct(product: Product) {
    editedProduct.value = JSON.parse(JSON.stringify(product));
    dialog.value = true;
  }
  return {
    products,
    getProducts,
    dialog,
    editedProduct,
    saveProduct,
    editProduct,
    deleteProduct,
    checkDialog,
    deleteDialog,
  };
});
