import type User from "@/types/User";
import { defineStore } from "pinia";
import { ref, watch } from "vue";
import userService from "@/services/user";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useUserStore = defineStore("User", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const checkDialog = ref();
  const deleteDialog = ref(false);
  const users = ref<User[]>([]);
  const editedUser = ref<User>({ login: "", name: "", password: "" });
  watch(dialog, (newdialog, oldDialog) => {
    console.log(newdialog);
    if (!newdialog) {
      editedUser.value = { login: "", name: "", password: "" };
    }
  });
  async function getUsers() {
    loadingStore.isloading = true;
    try {
      const res = await userService.getUsers();
      users.value = res.data;
      console.log(res);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล User ได้");
    }
    loadingStore.isloading = false;
  }
  async function saveUser() {
    loadingStore.isloading = true;
    try {
      if (editedUser.value.id) {
        const res = await userService.updateUser(
          editedUser.value.id,
          editedUser.value
        );
      } else {
        const res = await userService.saveUser(editedUser.value);
      }

      dialog.value = false;
      await getUsers();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก User ได้");
      console.log(e);
    }
    loadingStore.isloading = false;
  }
  async function deleteUser(id: number) {
    loadingStore.isloading = true;

    try {
      const res = await userService.deleteUser(id);
      await getUsers();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล User ได้");
    }
    loadingStore.isloading = false;
  }

  function editUser(user: User) {
    editedUser.value = JSON.parse(JSON.stringify(user));
    dialog.value = true;
  }
  return {
    dialog,
    users,
    getUsers,
    editedUser,
    saveUser,
    deleteUser,
    editUser,
    checkDialog,
    deleteDialog,
  };
});
